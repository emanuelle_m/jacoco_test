package com.example.emanuellemenali.jetpackexample

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        firstButton.setOnClickListener { text.text = "Hello World!" }
//        secButton.setOnClickListener { it.visibility = View.GONE }
    }
}
