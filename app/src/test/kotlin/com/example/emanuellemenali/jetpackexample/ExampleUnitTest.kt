package com.example.emanuellemenali.jetpackexample

import android.app.Activity
import android.widget.Button
import android.widget.TextView
import org.hamcrest.CoreMatchers.equalTo
import org.junit.Assert.assertEquals
import org.junit.Assert.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(RobolectricTestRunner::class)
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun clickButton_shouldChangeText() {
        val activity: Activity = Robolectric.setupActivity(MainActivity::class.java)

        val firstButton = activity.findViewById<Button>(R.id.firstButton)
        val text = activity.findViewById<TextView>(R.id.text)

        firstButton.performClick()

        assertThat(text.getText().toString(), equalTo("Hello World!"))
    }

}
